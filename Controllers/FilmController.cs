﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSharpApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CSharpApi.Controllers
{

    [Route("api/[controller]")]
    public class FilmController : Controller
    {
        private sakilaContext _context;

        public FilmController(sakilaContext context)
        {
            _context = context;
        }
        // GET api/film
        [HttpGet]
        public IActionResult Index()
        {
            var res = _context.Film
                .Include(p => p.FilmActor)
                .ThenInclude(filmActor => filmActor.Actor)
                .ToArray();

            /*  var json = JsonConvert.SerializeObject (res, Formatting.None,
                 new JsonSerializerSettings {
                     PreserveReferencesHandling = PreserveReferencesHandling.Objects
                     // PreserveReferencesHandling = PreserveReferencesHandling.All
                     // ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                 }); */
            var json = JsonConvert.SerializeObject(res, Formatting.None,
                new JsonSerializerSettings
                {
                    ContractResolver = new ManyToManyJson("Film", "Actor", "FilmActor")
                });
            return Ok(json);
        }
    }
}