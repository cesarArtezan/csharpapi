﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSharpApi.Models;
using CSharpApi.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CSharpApi.Controllers
{
    [Route("api/[controller]")]
    public class ActorsController : Controller
    {
        private sakilaContext _context;
        private IActorsRepository _actors;

        public ActorsController(sakilaContext context)
        {
            _context = context;
            _actors = new ActorsRepository(context);
        }
        // GET api/actors
        [HttpGet]
        public ActionResult Index()
        {
            return Ok(_actors.GetActors());

        }
        // GET api/actors/101
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            string actor = _actors.GetActorById(id);
            if (actor != "null")
            {
                return Ok(actor);
            }
            else
            {
                return NotFound();
            }
        }
        // POST api/actors
        [HttpPost]
        public IActionResult Post([FromBody]Actor actor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            int success = _actors.AddNewActor(actor);
            if (success == 1)
            {
                return Created("api/actors", actor);
            }
            return BadRequest();
        }
    }
}