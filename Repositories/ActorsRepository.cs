
using CSharpApi.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CSharpApi.Repositories
{
    public class ActorsRepository : IActorsRepository
    {
        private sakilaContext _context;
        public ActorsRepository(sakilaContext context)
        {
            _context = context;
        }

        public int AddNewActor(Actor actor)
        {
            int maxId = _context.Actor.Max(p => p.ActorId);
            actor.ActorId = maxId + 1;
            _context.Actor.Add(actor);
            return _context.SaveChanges();
        }

        public int DeleteActorById(int id)
        {
            throw new System.NotImplementedException();
        }

        public string GetActorById(int id)
        {
            Actor actor = _context.Actor
            .Include(p => p.FilmActor)
                .ThenInclude(filmActor => filmActor.Film)
            .SingleOrDefault(a => a.ActorId == id);
            string json = JsonConvert.SerializeObject(actor, Formatting.Indented,
             new JsonSerializerSettings
             {
                 ContractResolver = new ManyToManyJson("Actor", "Film", "FilmActor")
             });
            return json;
        }

        public string GetActors()
        {
            Actor[] res = _context.Actor
                .Include(p => p.FilmActor)
                .ThenInclude(filmActor => filmActor.Film)
                .ToArray();

            string json = JsonConvert.SerializeObject(res, Formatting.Indented,
             new JsonSerializerSettings
             {
                 ContractResolver = new ManyToManyJson("Actor", "Film", "FilmActor")
             });
            return json;
        }

        public int UpdateActorById(int id, Actor actor)
        {
            throw new System.NotImplementedException();
        }

        public int UpdateActorByIdEntityState(int id, Actor actor)
        {
            throw new System.NotImplementedException();
        }
    }

}