
using CSharpApi.Models;

namespace CSharpApi.Repositories
{
    public interface IActorsRepository
    {
        int AddNewActor(Actor actor);
        int DeleteActorById(int id);
        string GetActorById(int id);
        string GetActors();
        int UpdateActorById(int id, Actor actor);
        int UpdateActorByIdEntityState(int id, Actor actor);
    }
}