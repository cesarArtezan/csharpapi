﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CSharpApi.Models {
    public partial class Actor {
        public Actor () {
            FilmActor = new HashSet<FilmActor> ();
        }

        public int ActorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTimeOffset LastUpdate { get; set; }

        public virtual ICollection<FilmActor> FilmActor { get; set; }
    }
}