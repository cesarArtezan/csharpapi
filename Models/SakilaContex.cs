﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CSharpApi.Models
{
    public class sakilaContext : DbContext
    {
        public sakilaContext(DbContextOptions<sakilaContext> options) : base(options) { }

        public DbSet<Actor> Actor { get; set; }
        public DbSet<FilmActor> FilmActor { get; set; }
        public DbSet<Film> Film { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // ACTOR
            modelBuilder.Entity<Actor>(entity =>
            {
                entity.ToTable("actor", "sakila");

                entity.HasIndex(e => e.LastName)
                    .HasName("idx_actor_last_name");

                entity.Property(e => e.ActorId)
                    .HasColumnName("actor_id")
                    .HasColumnType("smallint(5) unsigned");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnName("first_name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("last_name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdate)
                    .HasColumnName("last_update")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });
            // FILM
            modelBuilder.Entity<Film>(entity =>
            {
                entity.ToTable("film", "sakila");

                entity.HasIndex(e => e.LanguageId)
                    .HasName("idx_fk_language_id");

                entity.HasIndex(e => e.OriginalLanguageId)
                    .HasName("idx_fk_original_language_id");

                entity.HasIndex(e => e.Title)
                    .HasName("idx_title");

                entity.Property(e => e.FilmId)
                    .HasColumnName("film_id")
                    .HasColumnType("smallint(5) unsigned");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .IsUnicode(false);

                entity.Property(e => e.LanguageId)
                    .HasColumnName("language_id")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.LastUpdate)
                    .HasColumnName("last_update")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Length)
                    .HasColumnName("length")
                    .HasColumnType("smallint(5) unsigned");

                entity.Property(e => e.OriginalLanguageId)
                    .HasColumnName("original_language_id")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.Rating)
                    .HasColumnName("rating")
                    .HasColumnType("enum('G','PG','PG-13','R','NC-17')")
                    .HasDefaultValueSql("G");

                entity.Property(e => e.ReleaseYear)
                    .HasColumnName("release_year")
                    .HasColumnType("year(4)");

                entity.Property(e => e.RentalDuration)
                    .HasColumnName("rental_duration")
                    .HasColumnType("tinyint(3) unsigned")
                    .HasDefaultValueSql("3");

                entity.Property(e => e.RentalRate)
                    .HasColumnName("rental_rate")
                    .HasColumnType("decimal(4,2)")
                    .HasDefaultValueSql("4.99");

                entity.Property(e => e.ReplacementCost)
                    .HasColumnName("replacement_cost")
                    .HasColumnType("decimal(5,2)")
                    .HasDefaultValueSql("19.99");

                entity.Property(e => e.SpecialFeatures)
                    .HasColumnName("special_features")
                    .HasColumnType("set('Trailers','Commentaries','Deleted Scenes','Behind the Scenes')");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                /*  entity.HasOne (d => d.Language)
                     .WithMany (p => p.FilmLanguage)
                     .HasForeignKey (d => d.LanguageId)
                     .OnDelete (DeleteBehavior.ClientSetNull)
                     .HasConstraintName ("fk_film_language");

                 entity.HasOne (d => d.OriginalLanguage)
                     .WithMany (p => p.FilmOriginalLanguage)
                     .HasForeignKey (d => d.OriginalLanguageId)
                     .HasConstraintName ("fk_film_language_original"); */
            });

            // Many to Many film => actor
            modelBuilder.Entity<FilmActor>(entity =>
            {
                entity.HasKey(e => new { e.ActorId, e.FilmId });

                entity.ToTable("film_actor", "sakila");

                entity.HasIndex(e => e.FilmId)
                    .HasName("idx_fk_film_id");

                entity.Property(e => e.ActorId)
                    .HasColumnName("actor_id")
                    .HasColumnType("smallint(5) unsigned");

                entity.Property(e => e.FilmId)
                    .HasColumnName("film_id")
                    .HasColumnType("smallint(5) unsigned");

                entity.Property(e => e.LastUpdate)
                    .HasColumnName("last_update")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(d => d.Actor)
                    .WithMany(p => p.FilmActor)
                    .HasForeignKey(d => d.ActorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_film_actor_actor");

                entity.HasOne(d => d.Film)
                    .WithMany(p => p.FilmActor)
                    .HasForeignKey(d => d.FilmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_film_actor_film");
            });
            /*   modelBuilder.Entity<FilmActor> ().HasKey (sc => new { sc.actor_id, sc.film_id });

            modelBuilder.Entity<FilmActor> ().ToTable ("film_actor", "sakila");

            modelBuilder.Entity<FilmActor> ()
                .HasOne<Actor> (sc => sc.Actor)
                .WithMany (s => s.FilmActor)
                .HasForeignKey (sc => sc.actor_id)
                .HasConstraintName ("fk_film_actor_actor");

            modelBuilder.Entity<FilmActor> ()
                .HasOne<Film> (sc => sc.Film)
                .WithMany (s => s.FilmActor)
                .HasForeignKey (sc => sc.film_id)
                .HasConstraintName ("fk_film_actor_film");
 */
        }
    }
}