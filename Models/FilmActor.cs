﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CSharpApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CSharpApi.Models
{
    public partial class FilmActor
    {
        public int ActorId { get; set; }
        public short FilmId { get; set; }
        public DateTimeOffset LastUpdate { get; set; }

        public virtual Actor Actor { get; set; }
        public virtual Film Film { get; set; }
    }
}

// Contract resolvers

public class FilmActorContractResolver : DefaultContractResolver
{
    bool _isActor;
    public FilmActorContractResolver(bool isActor)
    {
        _isActor = isActor;
    }
    protected override JsonProperty CreateProperty(
        MemberInfo member,
        MemberSerialization memberSerialization)
    {
        var property = base.CreateProperty(member, memberSerialization);
        bool isFilm = property.DeclaringType == typeof(Film);
        bool isActorType = property.DeclaringType == typeof(Actor);
        if ((_isActor && property.PropertyName == "Actor") ||
            (_isActor && isFilm && property.PropertyName == "FilmActor"))
        {
            property.ShouldSerialize = i => false;
        }
        else if ((!_isActor && property.PropertyName == "Film") ||
          (!_isActor && isActorType && property.PropertyName == "FilmActor"))
        {
            property.ShouldSerialize = i => false;
        }
        return property;
    }

}