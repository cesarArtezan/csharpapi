# Pasos MySQL

## Es .net Core

1.- Agregar `<PackageReference Include="MySql.Data.EntityFrameworkCore" Version="8.0.15" />`

2.- En Startup.cs `services.AddDbContext<SakilaContex>(options => options.UseMySQL(Configuration.GetConnectionString("SakilaDatabase")));`
3.- Crear Contex y model
4.- en appsettings.json

```json
{
  "ConnectionStrings": {
    "SakilaDatabase": "Server=localhost;Port=3306;Database=sakila;Uid=root;Pwd=123456;"
  },
  "IncludeScopes": false,
  "Logging": {
    "LogLevel": {
      "Default": "Warning"
    }
  },
  "AllowedHosts": "*"
}
```
