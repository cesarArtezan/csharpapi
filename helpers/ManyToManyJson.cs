using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CSharpApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
public class ManyToManyJson : DefaultContractResolver {
    public string _parentName;
    public string _soonName;
    public string _join;
    /// <summary>
    /// Modelo padre, hijo, tabla join
    /// </summary>
    public ManyToManyJson (string parentName, string soonName, string join) {
        _parentName = parentName;
        _soonName = soonName;
        _join = join;
    }
    protected override JsonProperty CreateProperty (
        MemberInfo member,
        MemberSerialization memberSerialization) {
        var property = base.CreateProperty (member, memberSerialization);

        var x = Type.GetType ("CSharpApi.Models." + _soonName);
        bool isSoonType = property.DeclaringType == Type.GetType ("CSharpApi.Models." + _soonName);
        bool isParentType = property.DeclaringType == Type.GetType ("CSharpApi.Models." + _parentName);
        if ((property.PropertyName == _parentName) ||
            (isSoonType && property.PropertyName == _join)) {
            property.ShouldSerialize = i => false;
        }
        return property;
    }

}